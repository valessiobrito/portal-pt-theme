(function($){
  function addSubtitles(){
    $('div.container-block-plugin_container-block > .block-inner-1').children('.block-inner-2').children('.block-title').wrap('<div class="block-header"></div>');
    $($('div.block-header')[0]).children('.block-title').append('<span class="subtitle">Colabore no passo a passo das consultas públicas e discussões de políticas públicas</span>');
    $($('div.block-header')[1]).children('.block-title').append('<span class="subtitle">Confira as últimas novidades do Participa.br</span>');
    $($('div.block-header')[2]).children('.block-title').append('<span class="subtitle">Faça parte</span>')
  }

  function addReadMoreOnTrackCards(){
    var cards = $('div.item_card');
    cards.each(function(){
      var link = $(this).children().attr('href');
      var readMore = $('<div class="read_more"><a><span>Leia mais</span></a></div>');
      $(readMore).children().attr('href', link);
      $($(this).find('.track_stats')).append(readMore);
    });
  }

  function translateButtons(){
   $('div.community-track-plugin_track-card-list-block').find('div.more a').text("Todas as Trilhas");
   $('a.view-all').each(function(){$(this).text('Veja todos');}); 
  }

  function insertRegisterMessage(){
    //insere a mensagem na página de registro//
    $( ".action-account-signup #content form" ).prepend( "<div class='singup_text'><p>Registre-se. Crie sua conta no Participa Brasil! Este é mais um espaço de diálogo entre governo e sociedade civil. Depois que você se registrar será possível fazer comentários e/ou contribuições, participar de consultas públicas, criar blogs, participar e/ou criar comunidades, etc.</p><p>A gestão pública nunca esteve tão próxima das pessoas como agora. Faça parte desta mudança!</p><p>Seja bem vind@!</p></div>" );
  }

  $(document).ready(function(){
    addSubtitles();
    addReadMoreOnTrackCards();
    translateButtons();
    insertRegisterMessage();
  });
})(jQuery);
